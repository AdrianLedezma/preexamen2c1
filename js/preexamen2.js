function cargarImgRaza() {
    const RazaSelec = document.getElementById("razaCanica").value;
    const url = "https://dog.ceo/api/breed/" + RazaSelec + "/images/random";

    fetch(url)
        .then(respuesta => respuesta.json())
        .then(datos => {
            const imagenURL = datos.message;
            document.getElementById("imgPerro").src = imagenURL;
        })
        .catch(error => {
            alert("Error al cargar la imagen.");
        });
}

function cargarRaza() {
    const RazaSelec = document.getElementById("razaCanica");

    fetch("https://dog.ceo/api/breeds/list")
        .then(respuesta => respuesta.json())
        .then(datos => {
            const razas = datos.message;
            RazaSelec.innerHTML = razas.map(raza => '<option value="' + raza + '">' + raza + '</option>').join('');
        })
        .catch(error => {
            alert("Error al cargar la raza.");
        });
}

document.getElementById("CargarRaza").addEventListener("click", function () {
    cargarRaza();
});

document.getElementById("VerImg").addEventListener("click", function () {
    cargarImgRaza();
});
